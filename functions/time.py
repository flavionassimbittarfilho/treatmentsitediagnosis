from datetime import datetime
from pytz import timezone


def setDatetimeNow(separatorDate='/', separatorTime=':'):
    fullDate = datetime.now().astimezone(timezone('America/Sao_Paulo'))
    date = fullDate.strftime(f'%d{separatorDate}%m{separatorDate}%Y')
    year = fullDate.strftime('%Y')
    month = fullDate.strftime('%m')
    time = fullDate.strftime(f'%H{separatorTime}%M')
    return {'date': date, 'year': year, 'month': month, 'time': time, 'fullDate':fullDate}
