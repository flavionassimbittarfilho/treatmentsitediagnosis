import openpyxl as xl
import pandas as pd
import os


def writerCSV(data, pathFileName, overwrite=False, sep=',', chunksize=1000 , language='br', encoding='utf-8'):
    theFileExist = os.path.exists(pathFileName)
    newDataFrame = pd.DataFrame(data)

    if language == 'br':
        decimal=','
        thousands='.'
    else:
        decimal='.'
        thousands=','

    if theFileExist:
        oldDataFrame = pd.read_csv(pathFileName, sep=sep, decimal=decimal, thousands=thousands, encoding=encoding)
        if not overwrite:        
            dataFrame = pd.concat([oldDataFrame,newDataFrame], axis=0, ignore_index=True)
        else:
            dataFrame = newDataFrame
    else:
        dataFrame = newDataFrame

    dataFrame.to_csv(pathFileName, sep=sep, encoding=encoding ,chunksize=chunksize, index=False)


def readCSV(pathFileName, sep=',', chunksize=1000 , language='br', encoding='utf-8'):

    if language == 'br':
        decimal=','
        thousands='.'
    else:
        decimal='.'
        thousands=','

    return pd.read_csv(pathFileName, sep=sep, decimal=decimal, thousands=thousands, encoding=encoding, chunksize=chunksize)


def writerExcel(data, pathFileName, tab):
    theFileExist = os.path.exists(pathFileName)
    newDataFrame = pd.DataFrame(data)

    if theFileExist:
        oldDataFrame = pd.read_excel(pathFileName)
        dataFrame = pd.concat([oldDataFrame,newDataFrame], axis=0, ignore_index=True)
        file = xl.load_workbook(pathFileName)

        if tab in file.sheetnames:
            mode = 'a'
            if_sheet_exists = 'overlay'
            startrow = 0
            header = True
        else:
            mode = 'a'
            if_sheet_exists = 'new'
            startrow = 0
            header = True
            
    else:
        dataFrame = newDataFrame
        mode = 'w'
        if_sheet_exists = None
        startrow = 0
        header = True



    with pd.ExcelWriter(pathFileName, mode=mode, if_sheet_exists=if_sheet_exists) as file:
        dataFrame.to_excel(file, sheet_name=tab,
                           startrow=startrow, index=False, header=header)
