#! ./.venv/bin/python3

from functions.time import setDatetimeNow
import pandas as pd
from functions.db import writerCSV


pd.set_option('display.max_columns', None)
fullDate = setDatetimeNow()
fileName = f"SiteDiagnosis{fullDate['year']}.csv"
pathFileName = f"/home/hacker-man/Documents/Rotinas/BD/Datalake/SiteDiagnosis/{fileName}"
dataFrame = pd.read_csv(pathFileName)

dataFrame['Extraction Date'] = pd.to_datetime(dataFrame['Extraction Date'],format='%Y-%m-%d')
dataFrame['Extraction Time'] = pd.to_datetime(dataFrame['Extraction Time'],format='%H:%M').dt.time

print('Tratando os dados...')
for column in dataFrame.columns[8:]:
       dataFrame[column] = dataFrame[column].replace('[^\.\d]', '' , regex=True).replace('', None).astype(float)
print('Dados tratados como:')
print(dataFrame.dtypes)

data = dataFrame.to_dict('list')
pathFileName = f'/home/hacker-man/Documents/Rotinas/BD/Dataset/{fileName}'

print(f'Salvando os dados tratados em\n{pathFileName}')
writerCSV(data, pathFileName, True)